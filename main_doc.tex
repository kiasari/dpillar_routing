\documentclass{llncs}
\usepackage[utf8]{inputenc}
\usepackage{amsmath, amssymb, amsfonts}
\usepackage{algorithm, algorithmicx, algpseudocode}
\usepackage{epic, graphicx, hyperref}
\usepackage{epstopdf}

\usepackage{pgfplots}
\usepackage{pgfplotstable}
\pgfplotsset{width=\linewidth,compat=1.8}

\graphicspath{{./figures/}}

\begin{document}



\author	{
	 Abbas Eslami Kiasari\inst{1}, Javier Navaridas\inst{1}, and Iain A. Stewart\inst{2}
	}

\institute { School of Computer Science, University of Manchester 
	  \\ Oxford Road, Manchester M13 9PL, U.K. \\  
	    \email{\{abbas.kiasari, javier.navaridas\}@manchester.ac.uk}\\
	    \and
	    School of Engineering and Computing Sciences, Durham University 
	   \\Science Lab, South Road, Durham DH1 3LE, U.K. \\
	    \email{i.a.stewart@durham.ac.uk}
 	   }

\title	{On Routing Algorithms for the \\DPillar Data Centre Networks }

\maketitle

\begin{abstract}
The DPillar data centre networks were introduced as an attractive topology for server-centric data centre networks and have recently received considerable attention. In this paper, we first derive analytically, and validate experimentally, the average hop count and the aggregate bottleneck throughput of the DPillar networks with a focus on single-path routing algorithms and the all-to-all traffic pattern. We use these models to explore the design space of the DPillar networks as a case study. In addition, we discuss the limitations of the original routing algorithms, showing that they do not benefit from the rich connectivity provided by the DPillar network and consequently do not tolerate link failures very well. To overcome these limitations we propose a collection of routing algorithms which keep the simplicity of the original but enable a more effective utilisation of the network. We empirically evaluate our proposed routing algorithms and we find that they outperform the original algorithms as regards network throughput ($\sim 2$x), average hop count ($\sim 5\%-10\%$), load balance and fault tolerance. 
\end{abstract}


\section{Introduction} \label{sec:int} 
\input{sec_int}

\section{Preliminaries} \label{sec:pre}
We define the DPillar topology, and then study the mathematical properties of its structure, which serve as the foundation of designing packet routing and forwarding mechanisms for DPillar.

\begin{figure}[!t]                 
   \centering
   \includegraphics[width=0.3\textwidth]{vertical}
   \caption{A vertical view of DPillar with $k$ columns of servers $(H_i)$ and $k$ columns of switches $(S_i)$}
   \label{fig:vertical}                
\end{figure}

\subsection{Topology} \label{subsec:topol}


An $(n, k)$ DPillar network is built from $k$ columns of servers $(H_i, 0 \leq i \leq k-1)$, comprising dual-port servers, and $k$ columns of $n$-port switches $(S_i, 0\leq i \leq k-1)$ where $n$ is even and $n \geq 4$. Each server column has $(n/2)^k$ servers and each switch column has $(n/2)^{k-1}$ switches. As shown in the Fig.~\ref{fig:vertical}, DPillar network can be imagined as columns of servers and columns of switches, arranged alternately and vertically on the surface of a cylindrical pillar, hence the name. An $(n, k)$ DPillar network connects $k(n/2)^k$ servers via $k(n/2)^{k-1}$ switches and has bisection width of $(n/2)^k$~\cite{liao2010dpillar}. One server in DPillar can be uniquely addressed as $(c, v_{k-1} v_{k-2} ... v_0)$ where $0 \leq c \leq k-1$ and $0 \leq v_i \leq n/2-1$. The first parameter, $c$, is the column-index and denotes the column in which the server resides, whilst the second parameter, $v_{k-1} v_{k-2} ... v_0$, is the row-index and denotes the position of the server within a column. Similarly, one switch can be uniquely identified as $(c, v_{k-2} v_{k-3} ... v_0)$. Note that the server row-index has $k$ coordinates $(v_{k-1} v_{k-2} ... v_0)$ whereas the switch row-index has $k-1$ coordinates $(v_{k-2} v_{k-3} ... v_0)$. Two servers in the server column $H_i$ are connected to the same switch in the column $S_i$ if their row-indices differ at the $i^{th}$ symbol only. Similarly, two servers in the server column $H_i$ are connected to the same switch in the column $S_{(i-1)\mod k}$ if their row-indices differ at the $((i-1)\mod k)^{th}$ symbol only. In the rest of the paper, for simplicity in the notation, we use $\oplus$ and $\ominus$ signs as modulo-$k$ addition and modulo-$k$ subtraction operations, respectively. More precisely, $i\oplus 1=(i+1)$ mod $k$ and $i\ominus 1=(i-1)$ mod $k$. Fig.~\ref{fig:DPillar_6_3} shows a (6,3) DPillar network in a two-dimensional view. It is worth mentioning that the servers in the rightmost and leftmost columns are identical but are shown separately to facilitate visualization.

%\begin{figure}[!t]                 
%	\centering
%	
%	\begin{tikzpicture}
%		\draw circle (2cm);
%		\fill [gray] (0:2) circle (0.2);
%		\fill [gray] (30:1.2) rectangle (1,1);
%		\fill [gray] (60:2) circle (0.2);
%		\fill [gray] (90:2) circle (0.2);
%		\fill [gray] (120:2) circle (0.2);
%		\fill [gray] (150:2) circle (0.2);
%		
%		\fill [gray] (240:2) circle (0.2);
%		\fill [gray] (270:2) circle (0.2);
%		
%		\node[text width=6cm, anchor=west, right] at (-0.3,1.5) {$H_0$};
%	\end{tikzpicture}
%
%	\caption{A vertical view of DPillar network}
%	\label{fig:DSE}
%\end{figure}	



\begin{figure}[!t]  
   \centering
   \includegraphics[width=0.9\textwidth]{DPillar_6_3.png}
   \caption{Two-dimensional view of a (6,3) DPillar network. The red lines show the route between servers 0,000 and 2,222 computed by the routing algorithm DPillarSP in the clockwise direction. Note that the rightmost and leftmost columns are the same.}
   \label{fig:DPillar_6_3}                
\end{figure}

\subsection{Single-Path Routing (DPillarSP)} \label{subsec:SP}
 
Thanks to the symmetry of the DPillar interconnection network, a straightforward packet routing was proposed whose main objective was to simplify its implementation in practical environments. In this canonical single-path routing algorithm (DPillarSP)~\cite{liao2012dpillar}, the next hop can be determined by continuously moving clockwise (or anti-clockwise) and replacing the $i^{th}$ (accordingly $(i-1)^{th}$) coordinate in the row-index of the current server with the corresponding symbol in the row-index of the destination server. 


Let $u$ and $v$ be consecutive servers on a route from source server $src$ to destination server $dst$, computed by DPillarSP. If the server $u$ in column $H_i$ forwards the packet to the server $v$ in the column $H_{i\oplus 1}$ (in a clockwise direction), the $i^{th}$ symbol of the row-index of $v$ and destination are the same. Similarly, if $u$ forwards the packet from $H_i$ to $H_{i\ominus 1}$ (in an anti-clockwise direction), the ${(i\ominus 1)}^{th}$ symbol of the row-index of $v$ and the destination are the same. By continuing to do this, the packet can be forwarded to a server whose row-index is the same as the destination’s row-index. After that, the packet can be sent to its destination by always forwarding to a next hop server with the same row-index. Note that in two neighbouring columns, servers with the same row-index are directly connected by a switch. Let’s give an example in the (6,3) DPillar network (Fig.~\ref{fig:DPillar_6_3}) to clarify the DPillarSP routing algorithm. The clockwise route for a packet from server (0, 000) to server (2, 222) is (0, 000), (1, 002), (2, 022), (0, 222), (1, 222), and (2, 222) and the anti-clockwise route is (0, 000), (2, 200), (1, 220), (0, 222), (2, 222). Fig.~\ref{fig:DPillar_6_3} shows the route in the clockwise direction.

\subsection{Multi-Path Routing (DPillarMP)} \label{subsubsec:MP}

In an $(n,k)$ DPillar network, each server is connected to $n/2$ servers via its clockwise neighbouring switch and $n/2$ servers via its anti-clockwise neighbouring switch. However, the routing algorithm DPillarSP uses only one of the neighbours of the source server and one of the neighbours of the destination server. In order to exploit the rich connections inside a DPillar network and to tolerate failures, the DPillar multi-path routing algorithm DPillarMP was proposed to provide $n/2$ disjoint paths between any pair of source and destination servers~\cite{liao2012dpillar}; in~\cite{liao2012dpillar}, paths from a source server to a destination server are disjoint if they do not have any common server or switch except the switches connected to the source and destination servers. The routing algorithm DPillarMP can be summarised in three steps: (1) forward a packet from the source server to a proxy source server which is one of the $n/2$ neighbours of the source server, (2) route the packet according to the routing algorithm DPillarSP from the proxy source server to a proxy destination server which is one of the $n/2$ neighbours of the destination server, and (3) simply forward the packet from the proxy destination server to the destination server. Fig.~\ref{fig:MP} shows an example of the routing algorithm DPillarMP. More details on how to pair a proxy source server with a proxy destination server can be found in~\cite{liao2012dpillar}. It has been shown that the diameter of DPillarMP in the $(n,k)$ DPillar network is $2k+1$ which is 2 hops more than diameter of DPillarSP~\cite{liao2012dpillar}. 

\begin{figure}[!t]                 
   \centering
   \includegraphics[width=0.6\textwidth]{MP}
   \caption{$n/2$ disjoint paths between any source and destination}
   \label{fig:MP}                
\end{figure}


\section{Performance of DPillarSP} \label{per}

In this section, we continue the study of the routing algorithm DPillarSP by investigating the average hop count and throughput of DPillarSP routing algorithm.

\subsection{Average Hop Count} \label{subsec:avg}

As the average hop count is an indicator of expected packet latency under a moderate workload, it is widely acknowledged as an important static parameter of interconnection networks. However, to the best of our knowledge, a formula for the average hop count in DPillar network has not been published yet. 
In this section, we derive an exact formula for the average hop count in the DPillar network under all-to-all (uniform random) traffic pattern and the DPillarSP routing algorithm. We define the average hop count in a DCN as the mean number of hops messages will need to travel between every pair of servers, including self-sent messages. In this work we assume that the distance between any neighbouring servers (server-switch-server) is one hop. Table~\ref{tab:notation} represents parameters and notations used in this study.


\begin{table}[!t]
   \centering
   \caption{Parameters and notations}
   \label{tab:notation}                
   \begin{tabular}{|l|l|}
      \hline
      $\oplus$ & modulo-$k$ addition \\ \hline
      $\ominus$ & modulo-$k$ subtraction \\ \hline
      $n$ & number of ports per switch \\ \hline
      $k$ & number of columns in the DPillar network \\ \hline
      $N$ & number of servers in the network \\ \hline
      $F$ & number of flows in the network \\ \hline
      $C$ & number of channels in the network	\\ \hline
      $N_i$ & number of servers which are $i$ hops away from a given server \\ \hline
      $\bar{d_c}$ & average number of channels traversed per route \\ \hline
      $\bar{d}$ & average hop count in the network \\ \hline
      $\gamma_{max}$ & maximum channel load in the network \\ \hline
      ABT & aggregate bottleneck throughput \\ \hline
      DPillarSP & single-path routing	algorithm~\cite{liao2012dpillar} \\ \hline
      DPillarMP & multi-path routing algorithm~\cite{liao2012dpillar} \\ \hline
      RND\_SP & random direction DPillarSP \\ \hline
      SHD\_SP & shorter direction DPillarSP \\ \hline
      RND\_MP & random direction DPillarMP \\ \hline
      SHD\_MP & shorter direction DPillarMP \\ \hline
      RND\_SP+TRN & RND\_SP with turn back \\ \hline
      SHD\_SP+TRN & RND\_SP with turn back \\ \hline
      RND\_MP+TRN & RND\_MP with turn back \\ \hline
      SHD\_MP+TRN & RND\_MP with turn back \\ \hline
   \end{tabular}
\end{table}

\begin{lemma} \label{lem:avg}
The average hop count in the $(n,k)$ DPillar network with the routing algorithm DPillarSP is $\frac{3k-1}{2}+\frac{1-(n/2)^{-k}}{1-(n/2)}$.
\end{lemma}

\begin{proof}
Consider the routing algorithm DPillarSP and let $N_i$ be the number of servers that are $i$ hops away from server $A = (c, v_{k-1} v_{k-2} ... v_{c+1} v_c v_{c-1} ... v_0)$. We have $0 \leq i \leq 2k-1$, since the maximum hop count in an $(n,k)$ DPillar when using DPillarSP is $2k-1$~\cite{liao2012dpillar}. We consider first the case when $0 \leq i \leq k-1$. It is obvious that $N_0$ represents self-sent traffic so, $N_0=1$. As mentioned in Section~\ref{subsec:SP}, the routing algorithm DPillarSP can forward a packet from server $A = (c, v_{k-1} v_{k-2} ... v_{c+1} v_c v_{c-1} ... v_0)$ to an adjacent server $B = (c \oplus 1, v_{k-1} v_{k-2} ... v_{c+1} x v_{c-1} ... v_0)$ in the clockwise direction with $0 \leq x \leq n/2-1$. Hence, there are $n/2$ possibilities for coordinate $c$ in the row-index of $B$. Consequently, $n/2$ servers are 1 hop away from server $A$ and all are reachable by the routing algorithm DPillarSP. Similarly, server $B$ forwards the packet to server $C = (c \oplus 2, v_{k-1} v_{k-2} ...v_{c+2} y x v_{c-1} ... v_0)$ with $0 \leq y \leq n/2-1$. It means that there are $n/2$ choices for the symbol in position $c$ and $n/2$ choices for the symbol in position $c+1$ in the row-index of $C$. Hence, $(n/2)^2$ servers are 2 hops away from server $A$ via server $B$. Generally, we can say that $(n/2)^i$ servers are $i$ hops away from a server when $0 \leq i \leq k-1$, i.e., during the first turn around the pillar.
	
Now we consider the case when $k \leq i \leq 2k-1$. In the case of $i=k$, packets travel $k$ hops and in each hop there are $n/2$ choices from $\{0, 1, 2, ..., n/2-1\}$ for one of the symbols in the row-index of intermediate servers. As a result, there are $(n/2)^k$ servers which are $k$ hops away from the source node but notice that one of these servers is the source node. Hence, we can say that $N_k=(n/2)^k-N_0=(n/2)^k-1$. 
	
Using the same approach, we can find $N_i$ when $k < i \leq 2k-1$. Packets visit the destination column twice, which take place at $i^{th}$ (last) hop and $(i-k)^{th}$ hop. Hence, to calculate $N_i$, we need to exclude the possible destinations in the first round; $N_i=(n/2)^k-N_{i-k}=(n/2)^k-(n/2)^{i-k}$. To put it in nutshell
	
\begin{equation} \label{eq:distribution}
   N_i=\left\{
   \begin{array}{lll}
      &\mbox{$(n/2)^i$} & \mbox{if } 0 \leq i \leq k-1 \\
      &\mbox{$(n/2)^k-(n/2)^{i-k}$} & \mbox{if } k \leq i \leq 2k-1 \\
   \end{array}\right.
\end{equation}
	
Applying the same procedure for DPillarSP in anti-clockwise direction results in the same formula. As we calculated the distribution of the path length, it is easy to find the average hop count in the network; $\bar{d}=\sum_{i=0}^{2k-1}{iN_i/N}$ in which $N$ is the number of servers in the DPillar network. We compute $\sum_{i=0}^{2k-1}{iN_i}$ by using sum identities~\cite{patashnik1989concrete}.
	
\begin{align}
   \nonumber \sum_{i=0}^{2k-1}{iN_i}&=\sum_{i=0}^{k-1}{i(n/2)^i}+\sum_{i=k}^{2k-1}{i\left((n/2)^k-(n/2)^{i-k}\right)}\\
   \nonumber &=\frac{k(3k-1)}{2}(n/2)^k-k\frac{(n/2)^k-1}{(n/2)-1}
\end{align}
	
Now we are able to compute $\bar{d}$.
	
\begin{equation} \label{eq:avg}
   \bar{d}=\frac{\sum_{i=0}^{2k-1}{iN_i}}{N}=\frac{3k-1}{2}+\frac{1-(n/2)^{-k}}{1-(n/2)}
\end{equation}
	
\end{proof}

It is worth mentioning that $-1 < \frac{1-(n/2)^{-k}}{1-(n/2)} < 0$. Using this fact, we can determine lower and upper bounds for the average hop count: 
$\frac{3k-3}{2}< \bar{d} < \frac{3k-1}{2}$. 

\subsection{Aggregate Bottleneck Throughput} \label{subsec:abt}

The aggregate bottleneck throughput (ABT) is a metric introduced in~\cite{guo2009bcube} and is well-suited to evaluate DCNs as it is based on the all-to-all traffic patterns typically found in such systems. The reasoning behind ABT is that the performance of an all-to-all operation is limited by its slowest flow, i.e., the flow with the lowest throughput. Since data centres are a form of stream processing and are therefore bandwidth limited, this is an extremely important performance metric. The ABT is defined as the total number of flows, $F$, over the maximum channel load, $\gamma_{max}$, which means $ABT=F/\gamma_{max}$. $\gamma_{max}$ is determined by the channel that carries the largest fraction of traffic and, in the case of a symmetric topology (such as DPillar), it can be calculated as $\gamma_{max}=\bar{d_c}F/C$~\cite{dally2004principles} where $\bar{d_c}$ is the average number of channels traversed per route, which equals $2\bar{d}$ because in Lemma~\ref{lem:avg}, we assume that the length of a server-switch-server hop is counted as 1. $C$ is the number of channels in the network that carry traffic, and in the case of the DPillarSP routing algorithm it is equal to twice the number of servers, as each server has 2 ports. Hence, we can calculate the aggregate bottleneck throughput as $ABT=F/\gamma_{max}=C/\bar{d_c}$. Substituting $\bar{d_c}$ and $C$ with $\bar{d}$ and the number of servers results in

\begin{equation} \label{eq:abt}
   ABT=\frac{k(n/2)^k}{\bar{d}}
\end{equation}


\section{Improving Routing Algorithms} \label{sec:improving}

The main problem of the routing algorithms DPillarSP and DPillarMP is that they route packets in a single direction around the ring (typically clockwise). Hence, half of the connection resources in the DPillar network (those going anti-clockwise) will be left unused. We propose to improve upon the routing algorithms DPillarSP and DPillarMP by utilising both the clockwise and anti-clockwise links.


\subsection{Random Direction DPillarSP}
\label{subsec:RND_SP}
Random direction DPillarSP (RND\_SP) randomly decides a direction (either clockwise or anti-clockwise) at injection time and then applies the DPillarSP in that direction. This way, RND\_SP ensures a balanced distribution of the traffic among the two directions. Because RND\_SP distributes traffic over a larger number of links (exactly twice) it can sustain considerably higher throughput than DPillarSP. However, RND\_SP is not able to improve upon the average hop count of DPillarSP. The rationale is that, as we showed in Lemma~\ref{lem:avg}, the average hop counts in both directions are the same, so choosing randomly between them leads to exactly the same value. The pseudocode of our random direction single-path routing algorithm, denoted as RND\_SP, is shown in Algorithm~\ref{alg:RND_SP}. This algorithm takes the address of the source server (\textit{src}) and the address of the destination server (\textit{dst}) and returns the path between source and destination servers (\textit{path}). RND\_SP is based on the routing algorithm DPillarSP which takes source (\textit{src}), destination (\textit{dst}) and direction (\textit{clockwise} or \textit{anti-clockwise}) and returns the path (\textit{path}) between source and destination servers. If the route is faulty then DPillarSP returns null. For fault tolerance purposes, if the route in the selected direction is faulty, we will try the route in the opposite direction.


\begin{algorithm}[!t]
   \caption{\textit{RND\_SP(src, dst)}}
   \label{alg:RND_SP}

   \begin{algorithmic}[1]
      \If{\textit{rand}() mod 2=0} \Comment{if the random number is even}
         \State \textit{path = DPillarSP(src, dst, clockwise)}
         \If{\textit{path = null}} \Comment{the clockwise path is faulty}
            \State \textit{path = DPillarSP(src, dst, anti-clockwise)}
         \EndIf  
      \Else \Comment{if the random number is odd}
         \State \textit{path = DPillarSP(src, dst, anti-clockwise)}
         \If{\textit{path = null}} \Comment{the anti-clockwise path is faulty}
            \State \textit{path = DPillarSP(src, dst, clockwise)}
         \EndIf
      \EndIf
      \State return \textit{path}
   \end{algorithmic}
\end{algorithm}



\subsection{Shorter Direction DPillarSP}
\label{subsec:SHD_MP}

Adding more intelligence to RND\_SP routing algorithms, we are able to reduce the average path length in the DPillar networks. We simply estimate which direction will be shorter and try it first. Then, if due to faults in servers, switches, or links the route is not available then we will try the route in the other direction. The shorter direction is estimated on the basis of information about column-indices of source and destination servers. Although this estimation is not always accurate (in some cases it can select a longer direction) it chooses the shortest path in most of the cases and avoids a more complex computation of the shortest path. As will be shown in the next section this simple approach substantially reduces the diameter and average hop count in the DPillar data centre networks. Algorithm~\ref{alg:SHD_SP} shows the pseudocode of shorter direction single-path routing algorithm, SHD\_SP.
 
\begin{algorithm}[!t]
\caption{\textit{SHD\_SP(src, dst)}}
\label{alg:SHD_SP}

\begin{algorithmic}[1]
   \State \textit{ cw\_hop = (dst.col - src.col + k) mod k} \Comment{estimate clockwise hop count}
   \State \textit{acw\_hop = (src.col - dst.col + k) mod k} \Comment{estimate anti-clockwise hop count}
   \If{\textit{cw\_hop $<$ acw\_hop}} \Comment{clockwise path is shorter}
      \State \textit{path = DPillarSP(src, dst, clockwise)}
      \If{\textit{path = null}} \Comment{the clockwise path is faulty}		
         \State \textit{path = DPillarSP(src, dst, anti-clockwise)}
      \EndIf  
   \ElsIf{\textit{acw\_hop $<$ cw\_hop}} \Comment{anti-clockwise path is shorter}
      \State \textit{path = DPillarSP(src, dst, anti-clockwise)}
      \If{\textit{path = null}} \Comment{the anti-clockwise path is faulty}
         \State \textit{path = DPillarSP(src, dst, clockwise)}
      \EndIf
   \Else \Comment{both hop counts are the same}
      \State \textit{path = RND\_SP(src, dst)}
   \EndIf
   \State return \textit{path}
\end{algorithmic}
\end{algorithm}

As already discussed, DPillarMP routes a packet according to the routing algorithm DPillarSP from a proxy source server to a proxy destination server. Using RND\_SP and SHD\_SP instead of DPillarSP in multi-path routing, we will benefit from load balancing and fault tolerance at the same time. We name the new algorithms RND\_MP and SHD\_MP, respectively.

\subsection{Turn back feature} \label{subsec:trn}

The routing algorithms DPillarSP and DPillarMP assume a server always forwards packets in one direction. However, it is possible to send packets in the reverse direction. Servers in two neighbouring columns with the same row-index are connected to the same switch. Hence, after forwarding a packet to an intermediate server whose row-index is the same as the row-index of the destination, the intermediate server estimates which direction will be shorter (similar to SHD\_SP). If the route in the reverse direction is shorter, the packet forwarding direction will be changed (turn back) to bypass the longer route.

\section{Experimental Results} \label{sec:exp}

We used an in-house developed software tool to carry out the experimental work. The tool provides implementations of the discussed routing algorithms and enables us to ascertain how they will compare against each other. The tool also undertakes a breadth-first search (BFS) which allows us to compute the length of the shortest path between any two servers and also to examine whether two servers become disconnected in the presence of link failures. The operation of the tool is as follows: for each flow in the workload, it computes the route using the routing algorithm and updates link utilisation accordingly. Then it reports a large number of statistics of interest about scalability (in terms of diameter, average hop count and ABT) and fault tolerance (in terms of node-to-node connectivity under link failures). 

Fig.~\ref{plot:avg_abt_eval} shows how the average hop count and ABT scale with the number of servers, for different values of $n$ and $k$ when using DPillarSP. These figures also confirm that our analytical model (Section~\ref{subsec:avg}) perfectly matches the experimental results. 


\begin{figure}[!t]
%  \centering
   \input{plot_hop_abt}
   \caption{The average hop count and the ABT for different size of DPillar DCNs when using DPillarSP, comparing experimental results with those derived in Lemma~\ref{lem:avg}}
   \label{plot:avg_abt_eval}                
\end{figure}


Once the correctness of the analytical models has been ascertained, we proceed to discuss on how these models can be used when designing a DPillar-based data centre. Basically, they can be employed to carry out a broad exploration of the design space which would otherwise be prohibited to do based on other form of empirical or practical data. A representative example of such exploration can be seen in Fig.~\ref{fig:DSE}. There, we show the average hop count and ABT of a wide range of DPillar sizes and switch radices. This information, together with cost and power estimates -- which are outside of the scope of this paper -- can be used to decide the \emph{best} configuration of the datacentre. Note that the \emph{best} may have different meanings in different contexts and, indeed, may possibly lead to a Pareto front in some cases.

\begin{figure}[!t] \label{fig:DSE}
   \centering
   \input{plot3_hop_abt}
   \caption{Design space exploration of DPillar DCN based on average hop count and ABT}
\end{figure}	



We next compare the performance of our proposed routing algorithms in the (64,3) DPillar DCN comprising 98304 servers and 3072 switches. Fig.~\ref{fig:max_avg} shows the maximum and average hop count and compares them with the results obtained from BFS which is used to find the shortest path between any source and destination. We can see that adding randomness to the canonical algorithms (RND\_SP and RND\_MP) does not make any change in diameter and average hop count. However, choosing shorter direction (SHD\_SP and SHD\_MP) results in lower diameter and average hop count, as we expected. These figures also show that the turn back feature can help to reduce both maximum and average hop count. Having compared the average hop count with diameter in each routing algorithm, we can infer that the hop count histogram in DPillar networks follows a skewed distribution with the tail on the left-hand side. 


\begin{figure}[!t]                 
   \centering
   \includegraphics[width=0.49\textwidth]{max_hop}
   \includegraphics[width=0.49\textwidth]{avg_hop}
   \caption{Diameter and average hop count of proposed and canonical routing algorithms}
   \label{fig:max_avg}                
\end{figure}

The above figures show a clear improvement ($\sim5\%-10\%$) in terms of hop counts over the routing algorithms DPillarSP and DPillarMP. Fig.~\ref{fig:abt_load} shows that when it comes to the aggregate bottleneck throughput the improvement is much more substantial ($\sim$2x). In the context of datacentres, whose performance is bandwidth limited, this can translate into a huge impact. 

\begin{figure}[!t]                 
   \centering
   \includegraphics[width=0.49\textwidth]{abt_hist}
   \includegraphics[width=0.49\textwidth]{load.png}
   \caption{ABT and load distribution of proposed and canonical routing algorithms}
   \label{fig:abt_load}                
\end{figure}

Our experimental results also show that in the algorithms DPillarSP and DPillarMP half of the channels are never used and the other half have exactly the same load which means that the traffic is perfectly distributed over these channels. The experimental results also reveal that in our proposed routing algorithms all channels have a very similar load that is very close to maximum load, which demonstrates very good load balancing properties. 

We have seen above that the single-path routing family (DPillarSP, RND\_SP, and SHD\_SP with and without turn back) can sustain a better performance than multi-path routing family (DPillarMP, RND\_MP, and SHD\_MP with and without turn back) in terms of scalability. However, in the context of large-scale data centres, better performance may not necessarily be sufficient if it is not accompanied by a high resistance to failures. The rationale for this is that when scaling up to hundreds of thousands of servers, failures are common with the mean-time between failures being as short as hours or even minutes. In other words, failures are ubiquitous and so the DCN should be able to deal with them and remain competitively operational. Any network whose performance degrades rapidly with the number of failures is unacceptable, even if it does provide the best performance in a fault-free environment. Fig.~\ref{fig:conn} shows how the (64,3) DPillar DCN is affected by link failures in the terms of connectivity. Looking at the obtained result from BFS, we can see that DPillar network offers a rich connection between servers. However, the routing algorithms DPillarSP and DPillarMP are not able to exploit this richness and in fact have a very poor resilience to failures. For a 20\% failure rate, less than 20\% of the system remains connected. This goes up to a still disappointing 64\% with DPillarMP. Our proposed MP algorithms keep a 87\% connectivity, much closer to the optimal value (96\%). 


\begin{figure}[!t]                 
   \centering
   \input{plot_conn}
   \caption{Percentage of pairs of nodes connected by different routing algorithms}
   \label{fig:conn}                
\end{figure}


\section{Conclusion} \label{sec:con}
\input{sec_con}

%\bibliographystyle{plain}
\bibliographystyle{splncs03}
\bibliography{bib_DCN}

\end{document}

